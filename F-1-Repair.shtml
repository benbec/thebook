<!DOCTYPE html><html lang="en"><head>
<title>F-1: Repair</title>
<link rel="stylesheet" href="../pbfba.css">
<meta charset="UTF-8">
<!-- https://www.youtube.com/watch?v=MOgBW3WKeCc -->
<style>
.newsp { column-count: 1 ; border : transparent solid 0px ; }
</style>
</head>
<body id="BDY">

<div class="dates"> <!--#config timefmt="%a,
%Y-%b-%d, %H:%M" -->
	EDIT: 2022.0328 19:12 — 1,276<br>
	UPLD: MAMP <!--#echo var="DATE_GMT" --> GMT<br>
<a href="https://www.powweb.com/controlpanel/advanced -tools/">../pbfba.css — </a><a href="https://electromontis.net/RWWH/%20Index.shtml"> | Public</a> </div>

<div style="line-height:3;"><br></div>

<div id="TOP"><nav>
	<span style="line-height:0.5;"><br></span>
	<span class="navb" style="line-height:1;">
<a href="#END">▼</a><a href="#PRE" style="color:#a00;">▼</a>
<a href="#BDY">↖</a><a href="#BCV">↘</a></span></nav></div>
<!-- ====================================== -->
<div style="display: inline-block;">
<div class="dil"><btn onclick="resizeText(+3)" />+</div>
<div class="dil"><btn onclick="resizeText(-3)" />-</div>
<div class="dil"><btn onclick="resizeText(16)" />r</div>
</div>
<!-- ====================================== -->

<div>
<div><h1>RELIGION – <span>F: Repair</span><br>
<h1 style="font-size: 24px;">
What it Was For; What Went Wrong; How to Fix It<br>
<h1 style="font-size: 16px; font-weight:bold;">
<a href="../_E/E-1-Status.shtml#TOP">&loarr;Bk E: Status</a>
&emsp;&emsp;&emsp;<span>&copy;1999—2020 Benjamin F. Becula</span>
&emsp;&emsp;&emsp;
<a href="../_A/A-1-Preludes.shtml#TOP">Bk A: Preludes&roarr;</a><br>
<a href="../_F/F21-PogosProblem.shtml#TOP">(Next) F21 Pogo's Problem&roarr;</a>
</h1></div>

<div id="BCV" style="position: relative; display: inline-block;
	left:-86px;">
	<a style="color:transparent">▼▽</a>
<a href="#TOP">↖</a><a href="#PRE">↘</a>
</div>
<figure style="--imscl:1.0;"> <img src="../Im/Fi/F-Front-new.png"
	style=" left: 118.5mm;
	display : block; position: absolute; top : 80mm;
	height  : 4.6in; width : 3.25in;
	border  : 1px solid black;"
	alt="Front cover, celadon, with a circle containint a nippled ewer from Thera, tan with brown painting of a head of wheat." />
</figure>
<figure style="--imscl:1.5;"> <img src="../Im/Fi/F-Back-new.png"
	style=" left: 36.5mm;
	display : block; position: absolute; top : 80mm;
	height  : 4.6in;  width : 3.25in;
	border  : 1px solid black;"
	alt="Back cover, with a couple of paragraphs of text." />
</figure>

<!-- A kluje to accommodate HTML/CSS's inability to deal with vertical spacing. -->
<span style="line-height:25;"><br></span>
<span style="line-height:1;"><br></span>

<div><nav><ul class="newsp">
<li id="lsf1">
<a href="../%20Index.shtml#TOP">F: Repair, click for navigation instructions</a></li></ul>

<div><nav><span id="PRE" class="navb">
	<a style="color:transparent">▼▽</a>
<a href="#BCV">↖</a><a href="#END">↘</a>
	</span></nav></div>

<h2 id="F-1"  style="text-align:center; color:#a00;">
Pr&eacute;cis F: Repair</a>
</h2>



<p>[S]ocial problems have to be answered with social solutions, and &hellip; the only effective response to our ongoing national catastrophe must be a completely new set of social ideals.  But to achieve that, we will need a new myth, one that will both value the accomplishments of the past and look to the potentials of the future.  — Cory Panshin
</p>

<p class="epi">This is not a time to think of national security. It is a time to think of planetary security — indeed, of planetary survival. — Michael Ruppert.
</p>


<p>This is not a time to think about personal salvation. It is a time to think about planetary salvation. Recall Hawking's question, still unanswered: 'How can we survive the next 100 years?' Naturally it is easier to dismiss Chapter 20‘s future headlines, and imagine that something will turn up to solve all problems. It is easier to believe that ritual acceptance of a savior or prophet absolves you of all other responsibilities. Comforting as such evasions may be, they guarantee disaster. No ritual or belief is going to save us. None of the gods variously prayed to in 1177 BCE saved its people. The Olympians didn’t save Athens from herself. No Archangel kept Jeanne d’Arc from the stake. YHVH didn’t intervene during the Shoah. Baby-sitting is not in Coyotl‘s job description.
</p>

<p>Rituals were invented to minimize processing and use the primitive brains of the Jurassic most efficiently, but they didn‘t save the dinosaurs from the Chicxulub impactor. Ritual may help organize your life, but don‘t expect more than that from it. Belief was invented to minimize thinking after the bicameral days, but hasn‘t saved us from exploitation by schemers. Belief <i>may</i> make you a better person, but don‘t ask more of it.
</p>

<p><a class="fl" href="../_F/F21-PogosPredicament.shtml#TOP"><b>Chapter 21, Pogo‘s Predicament</b></a>, reminds us that most of our problems are of our own making. Current religion is not helping us solve them. Some suggestions about short-, medium-, and long-term repairs are offered.
</p>

<p><a class="fl" href="../_F/F22-BloodyCentury.shtml#TOP"><b>Chapter 22, Gaian Repairs</b></a>,
notes that a practical substitute for faith in divine intervention is to recognize that instantiating Moral Justice is hard for us and will take time, so our first priority is a sustainable culture. The Gaian imperatives thus take precedence over the Eirenic. Lester Brown‘s <cite>Plan B</cite> and the <cite>Stern Report</cite> lay out most of what needs to be done, with the added suggestion that the only truly sustainable energy source is space-based solar power. Theologically, this step implies extending synthetic kinship to our planet.
</p>

<p><a class="fl" href="../_F/F23-GaianThermo.shtml#TOP"><b>Chapter 23, Gaian Thermo</b></a>, Explains, in simple terms, why we should have paid more attention to those annoying people who wanted us to put environmental protection ahead of maximum CEO wage theft.
</p>

<p><a class="fl" href="../_F/F24-EirenicRepairs.shtml#TOP"><b>Chapter 24, Eirenic Repairs</b></a>, suggests, as a useful replacement for ritual, concentration on the Eirenic aspects of religion and actuallly getting down to brass tacks and building Moral Justice into the culture. Inequality, bad law, poor education, corporate greed, mismanagement of science, and some aspects of religous philosophy all need attention. Theologically, this step implies taking seriously  the common denominator of our better religions, the Golden Rule.
</p>

<p><a class="fl" href="../_F/F25-MythAndVision.shtml#TOP"><b>Chapter 25, Myth and Vision</b></a>, examines the need for ‘nonscientific‘ ideas, suggests some myths that need destroying, offers some suggestions about replacements, and introduces a useful idea that is crying for its own mythopoet.
</p>

<p><a class="fl" href="../_F/F26-Recapitulation.shtml#TOP"><b>Chapter 26, Recapitulation</b></a>, reviews the argument of Books 1-6.</p>


<p><a class="fl" href="../_F/F27-Maybe.shtml#TOP"><b>Chapter 27, Maybe</b></a>, suggests a rather different interpretation of all that has lead up to this point.
</p>
	<span class="fr" style="color:black;">◼</span>
<br>

<hr><div id="END">
<span class="navb">
	<a href="#TOP">▲</a><a href="#PRE">↖</a></span>
</div>

<div class="cen">
<a href="../_E/E-1-Status.shtml#TOP">&loarr; Bk E: Status</a>
&emsp;&emsp;&emsp;
<a href="F21-PogosProblem.shtml">(Next) F21-Pogo’s Problem &roarr;</a>
&emsp;&emsp;&emsp;
<a href="../_A/A-1-Preludes.shtml#TOP">Bk A: Preludes &roarr;</a><br>
<hr></div>
<br></div></div>



<script
src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
     integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
     crossorigin="anonymous">
</script>

<script>
	function resizeText(multiplier) {
	if (document.body.style.fontSize == "") {
	document.body.style.fontSize = "16px";
	}
	else document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 2) + "px";
	};
</script>
<!-- https://davidwalsh.name/change-text-size-onclick-with-javascript -->
<qscript> // This prints jQuery version (3.6.1) on an alert notice
if (typeof jQuery != 'undefined') {
    // jQuery is loaded => print the version
    alert(jQuery.fn.jquery);      }
</script>

</body></html>
