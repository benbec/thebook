<!DOCTYPE html><html lang="en"><head>
<title>JacketBio</title>
<link rel="stylesheet" href="../pbfba.css">
<meta charset="UTF-8">
<style>
.floatright
	{
	float: right;
	width: 300px;
	margin: 0 -6px 10px 20px;
	}
p	{ text-align: justify ;
	  padding   : 3px ;}
</style>
</head>

<body id="BDY" >

<h1 style="text-align:center;">What We Know of the Author</h1></div>

			<div id="divContent" class="divClass">

<div><p>Benjamin F. 'Benbec' Becula’s expectations of society were warped by formative years (0~30) in the two most liberal/progressive (i.e., civilized) communities in the U.S.:  Berkeley and Cambridge. A high-school socioeconomic dropout, he worked at what was available (ditch digger, truck driver, welder, machinist, aircraft loftsman, machine designer, …). Driven by an abiding curiosity, he looked for answers in seminary, MIT (PhD, physical chemistry), and a scattered professorial career in thalassochemistry.</p>

<p>His working years (30~60) taught him that Science answers How Come? fairly well, but Why? not at all, and that Theology answers Why? from what can only be called an inadequate perspective. He devoted a third of his thinking years (60~90+) to Why? before (like many before him) reluctantly accepting a variety of projection: <b>both kinds of gods are real</b> and much like us — but in incompatible ways, <span class="rdb">and we have confused them</span>. His assessment of one sort finds them to have been essential in early days, but vanishing when no longer needed. The other sort is seeking experimental answers to a possibly insoluble problem, with us as colleagues or lab-rats (our choice).</b> Small wonder that we have fought over religion!
</p>

<div>
<p>Descended from hereditary bards of the Lord of the Isles, Becula weaves kinship with all life, the Enlightenment aspirations of the Founding Fathers, Gravesian
mytho-poesis, contemporary science, an unfettered imagination, and mathematical intuitions from Plato to Tegmark into an optimistic and explanatory <i><b>theory of religion</b></i>. Ideas essential to any contemporary theory, but apt to be missing from the 200-odd earlier theories which Becula summarizes include (among others) the:<p>


<div class="floatright">
<figure>
<img src="../Im/BFB.png" width="300" height="361"
	alt="BFB.png Three-quarter view from above,
	of a nonagenarian philosopher."  />
<figcaption>
No photograph is known of the reclusive Benjamin
Franklin Becula, whose auto-biography (other than
brief personal glimpses in footnotes) seems to
consist of, ‘I make a pretty good hermit’. We do,
however, have this charcoal sketch by the artist
known to one and all as O.B.
</figcaption>
</figure> </div>

<div class="floatleft">
<ul>
<li>Natural Philosophy of John Muir
<li>Endosymbiosis of Lynn Margulis
<li>Cosmology of Edward Fredkin
<li>Primatology of Allison Jolly
<li>Sociology of Herbert Gintis
<li>Isonomia of Anaximander
<li>Economics of Mondragón
<li>Theology of Akhnaton
<li>Epicurianism of Lucretius*
<li>Humanism of Thomas Paine
<li>Psychology of Wilhelm Reich
<li>Bicamerality of Julian Jaynes
<li>Gaian vision of James Lovelock
<li>Human ecology of Garrett Hardin
<li>And a nod to quantum field theory.
</ul>
</div>

<div>
<p>While these are unfamiliar and may seem unikely contributors to religious theory, he notes: ‘The point of higher education is critical thinking, to distinguish wheat from chaff. Consider what critical thinking and Karl Popper's empirical falsifiability have done for the many professional disciplines that comprise the modern world. What might politics and religion accomplish if we required of them the same <i>professionalism</i>, with rigorous research criteria and <b>rejection of failed hypotheses</b> that lie behind these working theories?"
</p>
</div>
<p class="flu">* Because of Christian misrepresentation of this philosophy, some early readers questioned the propriety of this item. One wonders if church leaders feared that Epicurus's "a cheerful poverty is an honorable state" might lead the flock to expect it of them.
</div>



<p class="flu">
<span>Go to:&emsp;&emsp;</span><span style="color:#800;">
<a class="fl" href="../%20Index.shtml">Start Here</a>&ensp;
<a class="fl" href="../_A/A00-Preface.shtml#TOC">Preface</a>&ensp;
<a class="fl" href="4-Rando.shtml">Images</a>&emsp;</span>
<br><br>

703 — 2022.0418 11:24
<br>

<!-- >Get Jquery from Google  -->
<script
src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
     integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
     crossorigin="anonymous">
</script>

</div></body></html>
